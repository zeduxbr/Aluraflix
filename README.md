# Repositório da Primeira Semana do Challenge DevOps - Alura
<p>&nbsp;</p>

## Introdução

Como desafio desta semana, foi pedido que transformássemos a aplicação de backend Aluraflix em um container, com isso, foi criado um arquivo **Dockerfile** e construido uma imagem apartir dele.

Para chegar a este arquivo, foi necessário entender como o programa funciona, isto é, as portas TCP que o mesmo utiliza para se comunicar, a instalação de pré-requisitos para a execução do mesmo e etc.

<p>&nbsp;</p>

## Passos para a criação da imagem

Para a criação da imagem do Docker, dentro da pasta do projeto, crie uma virtualenv com o comando abaixo:

```zsh
virtualenv Aluraflix
``` 

Feito isso, ative a virtualenv com o comando abaixo:

```zsh
source Aluraflix/bin/activate
```

Após a ativação do virtualenv, temos que instalar os pacotes requeridos pela aplicação com o comando abaixo:

```zsh
pip install -r requirements.txt
```

O comando acima instala as dependências para que o mesmo funcione sem problemas.

O próximo passo é a criação da imagem com o comando abaixo:

```zsh
docker built -t Aluraflix .
```

Acompanhe a execução do comando e verifique se tudo ocorreu bem, caso tenha dado algum erro, verifique se você seguiu corretamente os passos anteriores.

<p>&nbsp;</p>

## Criação do conteiner da aplicação Aluraflix

Com a imagem criada, o próximo passo é criarmos o container e executá-lo. Para isso, execute o comando abaixo:

```zsh
docker run -d -p 8000:8000 --name Aluraflix Aluraflix
```

Com isso, será criado e executado o container, abra o seu navegador no endereço http://localhost:8000 e verifique que o mesmo será exibido a tela da aplicação.

Para testarmos se o super usuário criado na imagem esta funcionando, coloque o endereço http://localhost:8000/admin no seu navegador e verifique que será pedido um usuário e senha para o mesmo. Use **admin** para o campo usuário e **admin123** para o campo senha.


